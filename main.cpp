///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file main.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>
#include <list>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "random.hpp"
#include "animalFactory.hpp"

using namespace std;
using namespace animalfarm;

int main() {

	cout << "Welcome to Animal Farm 3" << endl;
	AnimalFactory testAnimal;
   
   //animal array
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );

   for(int i = 0; i < 25; i++)
   {
      animalArray[i] = testAnimal.pRandomAnimal();
     //cout << animalArray[i]->speak() << endl;
   }
   cout << endl << "Array of Animals:" << endl;
   cout << boolalpha;
   cout << "Is it empty:   " << animalArray.empty() << endl;
   cout << "Number of elements:  " << animalArray.size() << endl;
   cout << "Max size:   " << animalArray.max_size() << endl;

   for(Animal* animal : animalArray ) {
      if(animal != NULL){
         cout << animal->speak() << endl;
      }
   }

   for(Animal* animal : animalArray ) {
      if(animal != NULL){
         delete( animal );
      }
   }
   //animalArray.fill(NULL);

   //test code
   //cout << endl << "Is it empty:   " << animalArray.empty() << endl;
   //cout << "Number of elements:  " << animalArray.size() << endl;

   //animal list
   list<Animal*> animalList;
   list<Animal*>::iterator it1;

   for(int i = 0; i < 25; i++)
   {
      animalList.push_front(testAnimal.pRandomAnimal());
   }
   cout << endl << "List of Animals:" << endl;
   cout << boolalpha;
   cout << "Is it empty:   " << animalList.empty() << endl;
   cout << "Number of elements:  " << animalList.size() << endl;
   cout << "Max size:   " << animalList.max_size() << endl;

   for(Animal* animal : animalList ) {
      cout << animal->speak() << endl;
   }

   for(Animal* animal : animalList ) {
      if(animal != NULL){
         delete( animal );
      }
   }

   int animalListSize = animalList.size();

   for(int i = 0; i < animalListSize; i++)
   {
         it1 = animalList.begin();
         animalList.erase(it1);
   }

   //test code
   //cout << endl <<  "Number of elements:  " << animalList.size() << endl;
   //cout << "Is it empty:   " << animalList.empty() << endl;

	return 0;
}
