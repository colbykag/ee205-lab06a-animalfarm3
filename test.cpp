///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file test.cpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Colby Kagamida <colbykag@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   18_Feb_2021
///////////////////////////////////////////////////////////////////////////////

#include <iostream>
#include <array>

#include "animal.hpp"
#include "cat.hpp"
#include "dog.hpp"
#include "nunu.hpp"
#include "aku.hpp"
#include "palila.hpp"
#include "nene.hpp"
#include "random.hpp"
#include "animalFactory.hpp"
using namespace std;
using namespace animalfarm;

/*class AnimalFactory {
public: 
   AnimalFactory();
   static Animal* pRandomAnimal();

};

AnimalFactory::AnimalFactory(){
}

Animal* AnimalFactory::pRandomAnimal() {
   Random randomAni;
   Animal* pRanAnimal;
   randomAni.setParams(s,a,c,m);
   s = randomAni.scgRand();
   int pickRandAni = (s%67) % 6;
   switch(pickRandAni) {
      case 0:
         pRanAnimal = new Cat( pRanAnimal->getRandomName(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 1:
         pRanAnimal = new Dog( pRanAnimal->getRandomName(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 2:
         pRanAnimal = new Nunu( pRanAnimal->getRandomBool(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 3:
         pRanAnimal = new Aku( pRanAnimal->getRandomWeight(12,18),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      case 4:
         pRanAnimal = new Palila( pRanAnimal->getRandomName(),pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );
         break;
      default: 
         pRanAnimal = new Nene( pRanAnimal->getRandomName(), pRanAnimal->getRandomColor(), pRanAnimal->getRandomGender() );

   }
   return pRanAnimal;

}*/


int main() {

   //test stuff delete this later
   /*float toWei = 18;
   float fromWei = 12;
   for(int i = 0; i < 10; i++){
      enum Gender testGender = myCat.getRandomGender();
      enum Color testColor = myCat.getRandomColor();
      myCat.gender = testGender;
      myCat.hairColor = testColor;
      myCat.name = myCat.getRandomName();
      myNunu.isNative = myNunu.getRandomBool();
      myAku.weight = myAku.getRandomWeight(fromWei,toWei);
      cout <<" random Gender " << i << ":"<< myCat.genderName(testGender) << endl;
      cout <<" random Color " << i << ":"<< myCat.colorName(testColor) << endl;
      cout << boolalpha;
      cout <<" random Bool " << i << ":"<< myNunu.isNative << endl;
      cout <<" random Weight " << i << ":"<< myAku.weight << endl;
      cout <<" Random Name " << i << ":" << myCat.name << endl;
   }*/
   AnimalFactory testAnimal;
   /*for(int i = 0; i < 25; i++){
   Animal* pRandoAnimal = testAnimal.pRandomAnimal();
   cout << pRandoAnimal->speak() << endl;*/
   //pRandoAnimal->printInfo();
   //}
   //end of test stuff
   array<Animal*, 30> animalArray;
   animalArray.fill( NULL );
   for(int i = 0; i < 25; i++)
   {
      animalArray[i] = testAnimal.pRandomAnimal();
     cout << animalArray[i]->speak() << endl;
   }


	return 0;
}
